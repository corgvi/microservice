package com.coucode.notificationservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

@SpringBootApplication
@RestController
@EnableBinding(Sink.class)
public class NotificationserviceApplication {

    private Logger logger = LoggerFactory.getLogger(NotificationserviceApplication.class);

    @Autowired
    private WebClient.Builder builder;

    @Autowired
    private CircuitBreakerFactory circuitBreakerFactory;

    @StreamListener(Sink.INPUT)
    public void consumeMessage(Message message) {
        EmployeeResponseModel employeeResponseModel = circuitBreakerFactory.create("getEmployee").run(
                () -> {
                    EmployeeResponseModel model = builder.build()
                            .get()
                            .uri("http://localhost:9002/api/v1/employees/" + message.getEmployeeId())
                            .retrieve()
                            .bodyToMono(EmployeeResponseModel.class)
                            .block();
                    return model;
                },
                t -> {
                    EmployeeResponseModel model = new EmployeeResponseModel();
                    model.setFirstName("Anonymous");
                    model.setLastName("Employee");
                    return model;
                }
        );
        if (employeeResponseModel != null) {
            logger.info("Consume payload: " + employeeResponseModel.getFirstName() + " " + employeeResponseModel.getLastName() + " " + message.getMessage());
        }
    }

    public static void main(String[] args) {
        SpringApplication.run(NotificationserviceApplication.class, args);
    }

}
