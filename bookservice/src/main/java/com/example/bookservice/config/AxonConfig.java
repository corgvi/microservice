//package com.example.bookservice.config;
//
//import com.thoughtworks.xstream.XStream;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration
//public class AxonConfig {
//    @Bean
//    public XStream xStream() {
//        XStream stream = new XStream();
//        stream.allowTypesByWildcard(new String[] {
//                "com.coucode.**"
//        });
//        return stream;
//    }
//}
