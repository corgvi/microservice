package com.example.bookservice.query.controller;

import com.example.bookservice.query.model.BookResponseModel;
import com.example.bookservice.query.queries.GetAllBookQuery;
import com.example.bookservice.query.queries.GetBookQuery;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/books")
public class BookQueyController {

    @Autowired
    private QueryGateway queryGateway;

    @GetMapping("/{id}")
    public BookResponseModel getBookDetail(@PathVariable String id) {
        GetBookQuery getBookQuery = new GetBookQuery();
        getBookQuery.setId(id);

        BookResponseModel bookResponseModel = queryGateway.
                query(getBookQuery, ResponseTypes.instanceOf(BookResponseModel.class)).join();
        return bookResponseModel;
    }

    @GetMapping
    public List<BookResponseModel> findAll() {
        GetAllBookQuery getAllBookQuery = new GetAllBookQuery();
        List<BookResponseModel> list = queryGateway.
                query(getAllBookQuery, ResponseTypes.multipleInstancesOf(BookResponseModel.class)).join();
        return list;
    }
}
