package com.example.bookservice.command.event;

import com.example.bookservice.command.data.Book;
import com.example.bookservice.command.data.BookRepository;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;

@Component
public class BookEventHandler {
    @Autowired
    private BookRepository bookRepository;

    @EventHandler
    public void on (BookCreateEvent bookCreateEvent) {
        Book book = new Book();
        BeanUtils.copyProperties(bookCreateEvent, book);
        bookRepository.save(book);
    }

    @EventHandler
    public void on (BookUpdateEvent bookUpdateEvent) {
        Book book = bookRepository.findById(bookUpdateEvent.getId()).orElseThrow(() -> new EntityNotFoundException());
        BeanUtils.copyProperties(bookUpdateEvent, book);
        bookRepository.save(book);
    }

    @EventHandler
    public void on (BookDeleteEvent bookDeleteEvent) {
        bookRepository.deleteById(bookDeleteEvent.getId());
    }
}
