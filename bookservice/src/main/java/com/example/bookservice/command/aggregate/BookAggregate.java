package com.example.bookservice.command.aggregate;

import com.example.bookservice.command.command.CreateBookCommand;
import com.example.bookservice.command.command.DeleteBookCommand;
import com.example.bookservice.command.command.UpdateBookCommand;
import com.example.bookservice.command.event.BookCreateEvent;
import com.example.bookservice.command.event.BookDeleteEvent;
import com.example.bookservice.command.event.BookUpdateEvent;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;
import org.springframework.beans.BeanUtils;

@Aggregate
public class BookAggregate {
    @AggregateIdentifier
    private String id;
    private String name;
    private String author;
    private Boolean isReady;

    public BookAggregate() {
    }

    @CommandHandler
    public BookAggregate(CreateBookCommand createBookCommand) {
        // 1 sự kiện được tạo ra sau khi xử lý lên CreateBookCommand
        BookCreateEvent bookCreateEvent = new BookCreateEvent();
        // sao chép các thuộc tính từ createBookCommand vào bookCreateEvent
        BeanUtils.copyProperties(createBookCommand, bookCreateEvent);
        // phát đi event và gọi đến EventSourcingHandler
        AggregateLifecycle.apply(bookCreateEvent);
    }

    @CommandHandler
    public void handle(UpdateBookCommand updateBookCommand) {
        BookUpdateEvent event = new BookUpdateEvent();
        BeanUtils.copyProperties(updateBookCommand, event);
        AggregateLifecycle.apply(event);
    }

    @CommandHandler
    public void handle(DeleteBookCommand deleteBookCommand) {
        BookDeleteEvent event = new BookDeleteEvent();
        BeanUtils.copyProperties(deleteBookCommand, event);
        AggregateLifecycle.apply(event);
    }

    @EventSourcingHandler
    public void on(BookCreateEvent bookCreateEvent) {
        this.id = bookCreateEvent.getId();
        this.author = bookCreateEvent.getAuthor();
        this.name = bookCreateEvent.getName();
        this.isReady = bookCreateEvent.getIsReady();
     }

    @EventSourcingHandler
    public void on(BookUpdateEvent bookUpdateEvent) {
        this.id = bookUpdateEvent.getId();
        this.author = bookUpdateEvent.getAuthor();
        this.name = bookUpdateEvent.getName();
        this.isReady = bookUpdateEvent.getIsReady();
     }

    @EventSourcingHandler
    public void on(BookDeleteEvent bookDeleteEvent) {
        this.id = bookDeleteEvent.getId();
     }
}
