package com.example.bookservice.command.command;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateBookCommand {
    @TargetAggregateIdentifier
    private String id;
    private String name;
    private String author;
    private Boolean isReady;

//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getAuthor() {
//        return author;
//    }
//
//    public void setAuthor(String author) {
//        this.author = author;
//    }
//
//    public Boolean getIsReady() {
//        return isReady;
//    }
//
//    public void setIsReady(Boolean isReady) {
//        this.isReady = isReady;
//    }
//
//    public UpdateBookCommand() {
//    }
//
//    public UpdateBookCommand(String name, String author, Boolean isReady) {
//        this.name = name;
//        this.author = author;
//        this.isReady = isReady;
//    }
}
