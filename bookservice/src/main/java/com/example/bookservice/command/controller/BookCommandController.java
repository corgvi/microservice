package com.example.bookservice.command.controller;

import com.example.bookservice.command.command.CreateBookCommand;
import com.example.bookservice.command.command.DeleteBookCommand;
import com.example.bookservice.command.command.UpdateBookCommand;
import com.example.bookservice.command.model.BookRequestModel;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/books")
public class BookCommandController {
    @Autowired
    private CommandGateway commandGateway;

    @PostMapping
    public CreateBookCommand addBook(@RequestBody BookRequestModel bookRequestModel){
        // đóng gói BookRequestModel thành 1 message
        CreateBookCommand createBookCommand = new CreateBookCommand();
        createBookCommand.setIsReady(bookRequestModel.getIsReady());
        createBookCommand.setAuthor(bookRequestModel.getAuthor());
        createBookCommand.setId(UUID.randomUUID().toString());
        createBookCommand.setName(bookRequestModel.getName());
        // gửi đi
        commandGateway.sendAndWait(createBookCommand);
        return createBookCommand;
    }

    @PutMapping("/{id}")
    public UpdateBookCommand updateBook(@RequestBody BookRequestModel bookRequestModel, @PathVariable String id){
        // đóng gói BookRequestModel thành 1 message

        UpdateBookCommand updateBookCommand = new UpdateBookCommand();
        updateBookCommand.setId(id);
        updateBookCommand.setIsReady(bookRequestModel.getIsReady());
        updateBookCommand.setAuthor(bookRequestModel.getAuthor());
        updateBookCommand.setName(bookRequestModel.getName());
        // gửi đi
        commandGateway.sendAndWait(updateBookCommand);
        return updateBookCommand;
    }

    @DeleteMapping("/{id}")
    public DeleteBookCommand deleteBook(@PathVariable String id) {
        DeleteBookCommand deleteBookCommand = new DeleteBookCommand(id);
        commandGateway.sendAndWait(deleteBookCommand);
        return deleteBookCommand;
    }
}
