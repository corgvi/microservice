package com.example.bookservice.command.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookRequestModel {
    private Long id;
    private String name;
    private String author;
    private Boolean isReady;

//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getAuthor() {
//        return author;
//    }
//
//    public void setAuthor(String author) {
//        this.author = author;
//    }
//
//    public Boolean getIsReady() {
//        return isReady;
//    }
//
//    public void setIsReady(Boolean isReady) {
//        this.isReady = isReady;
//    }
//
//    public BookRequestModel() {
//    }
//
//    public BookRequestModel(Long id, String name, String author, Boolean isReady) {
//        this.id = id;
//        this.name = name;
//        this.author = author;
//        this.isReady = isReady;
//    }
}
