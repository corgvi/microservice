package com.coucode.borrowservice.command.event;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SendMessageEvent {
    private String id;
    private String employeeId;
    private String message;
}
