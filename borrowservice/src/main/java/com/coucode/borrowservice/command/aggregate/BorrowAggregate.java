package com.coucode.borrowservice.command.aggregate;

import com.coucode.borrowservice.command.command.CreateBorrowCommand;
import com.coucode.borrowservice.command.command.DeleteBorrowCommand;
import com.coucode.borrowservice.command.command.SendMessageCommand;
import com.coucode.borrowservice.command.command.UpdateBorrowCommand;
import com.coucode.borrowservice.command.event.CreateBorrowEvent;
import com.coucode.borrowservice.command.event.DeleteBorrowEvent;
import com.coucode.borrowservice.command.event.SendMessageEvent;
import com.coucode.borrowservice.command.event.UpdateBorrowEvent;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;
import org.springframework.beans.BeanUtils;

import java.util.Date;

@Aggregate
public class BorrowAggregate {
    @AggregateIdentifier
    private String id;
    private String employeeId;
    private String bookId;
    private Date borrowDate;
    private Date returnDate;
    private String message;

    public BorrowAggregate() {
    }

    @CommandHandler
    public BorrowAggregate(CreateBorrowCommand command) {
        CreateBorrowEvent event = new CreateBorrowEvent();
        BeanUtils.copyProperties(command, event);
        AggregateLifecycle.apply(event);
    }

    @CommandHandler
    public void handle(UpdateBorrowCommand command) {
        UpdateBorrowEvent event = new UpdateBorrowEvent();
        BeanUtils.copyProperties(command, event);
        AggregateLifecycle.apply(event);
    }

    @CommandHandler
    public void handle(DeleteBorrowCommand command) {
        DeleteBorrowEvent event = new DeleteBorrowEvent();
        BeanUtils.copyProperties(command, event);
        AggregateLifecycle.apply(event);
    }

    @CommandHandler
    public void handle(SendMessageCommand command) {
        SendMessageEvent event = new SendMessageEvent();
        BeanUtils.copyProperties(command, event);
        AggregateLifecycle.apply(event);
    }

    @EventSourcingHandler
    public void on(CreateBorrowEvent event) {
        this.id = event.getId();
        this.bookId = event.getBookId();
        this.employeeId = event.getEmployeeId();
        this.borrowDate = event.getBorrowingDate();
    }

    @EventSourcingHandler
    public void on(UpdateBorrowEvent event) {
        this.id = event.getId();
        this.bookId = event.getBookId();
        this.employeeId = event.getEmployeeId();
        this.returnDate = event.getReturnDate();
    }

    @EventSourcingHandler
    public void on(DeleteBorrowEvent event) {
        this.id = event.getId();
    }

    @EventSourcingHandler
    public void on(SendMessageEvent event) {
        this.id = event.getId();
        this.employeeId = event.getEmployeeId();
        this.message = event.getMessage();
    }
}
