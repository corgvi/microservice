package com.coucode.borrowservice.command.service;

import com.coucode.borrowservice.command.data.BorrowingRepository;
import com.coucode.borrowservice.command.model.Message;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
@EnableBinding(Source.class)
public class BorrowService {
    @Autowired
    private BorrowingRepository borrowingRepository;
    @Qualifier("output")
    @Autowired
    private MessageChannel messageChannel;

    public void sendMessage(Message message) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(message);
            messageChannel.send(MessageBuilder.withPayload(json).build());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public String findIdBorrowing(String employeeId, String bookId) {
        return borrowingRepository.findByEmployeeIdAndBookIdAndReturnDateIsNull(employeeId, bookId).getId();
    }
}
