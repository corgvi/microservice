package com.coucode.borrowservice.command.controller;

import com.coucode.borrowservice.command.command.CreateBorrowCommand;
import com.coucode.borrowservice.command.command.UpdateBorrowCommand;
import com.coucode.borrowservice.command.model.BorrowingRequestModel;
import com.coucode.borrowservice.command.service.BorrowService;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.UUID;

@RestController
@RequestMapping("api/v1/borrows")
public class BorrowController {
    @Autowired
    private CommandGateway commandGateway;
    @Autowired
    private BorrowService borrowService;

    @PostMapping
    public CreateBorrowCommand addBookBorrowing(@RequestBody BorrowingRequestModel model) {
        CreateBorrowCommand command = new CreateBorrowCommand();
        command.setId(UUID.randomUUID().toString());
        command.setBookId(model.getBookId());
        command.setEmployeeId(model.getEmployeeId());
        command.setBorrowingDate(new Date());
        commandGateway.sendAndWait(command);
        return command;
    }

    @PutMapping("/book/{bookId}/employee/{employeeId}")
    public UpdateBorrowCommand updateBookBorrowing(@RequestBody BorrowingRequestModel model, @PathVariable String bookId, @PathVariable String employeeId) {
        UpdateBorrowCommand command = new UpdateBorrowCommand();
        command.setId(borrowService.findIdBorrowing(employeeId, bookId));
        command.setBookId(bookId);
        command.setEmployeeId(employeeId);
        command.setReturnDate(new Date());
        commandGateway.sendAndWait(command);
        return command;
    }

}
