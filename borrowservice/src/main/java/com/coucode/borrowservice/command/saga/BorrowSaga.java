package com.coucode.borrowservice.command.saga;


import com.coucode.borrowservice.command.event.CreateBorrowEvent;
import com.coucode.commonservice.query.GetDetailsBookQuery;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.modelling.saga.SagaEventHandler;
import org.axonframework.modelling.saga.SagaLifecycle;
import org.axonframework.modelling.saga.StartSaga;
import org.axonframework.queryhandling.QueryGateway;
import org.axonframework.spring.stereotype.Saga;
import org.springframework.beans.factory.annotation.Autowired;
@Saga
public class BorrowSaga {
    @Autowired
    private CommandGateway commandGateway;
    @Autowired
    private QueryGateway queryGateway;

    @StartSaga
    @SagaEventHandler(associationProperty = "id")
    private void handle(CreateBorrowEvent event) {
        System.out.println("CreateBorrowEvent in Saga for BookId: " + event.getBookId() + " - EmployeeId: " + event.getEmployeeId());
//        try {
//            SagaLifecycle.associateWith("bookId", event.getBookId());
        GetDetailsBookQuery getDetailsBookQuery = new GetDetailsBookQuery(event.getBookId());
//            GetDetailsBookQuery
//        } catch () {
//
//        }
    }
}
