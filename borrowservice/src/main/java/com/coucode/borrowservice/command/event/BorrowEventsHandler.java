package com.coucode.borrowservice.command.event;

import com.coucode.borrowservice.command.data.Borrowing;
import com.coucode.borrowservice.command.data.BorrowingRepository;
import com.coucode.borrowservice.command.model.Message;
import com.coucode.borrowservice.command.service.BorrowService;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;

@Component
public class BorrowEventsHandler {
    @Autowired
    private BorrowingRepository borrowingRepository;

    @Autowired
    private BorrowService borrowService;

    @EventHandler
    public void on(CreateBorrowEvent event) {
        Borrowing borrowing = new Borrowing();
        BeanUtils.copyProperties(event, borrowing);
        borrowingRepository.save(borrowing);
    }

    @EventHandler
    public void on(DeleteBorrowEvent event) {
        if (borrowingRepository.findById(event.getId()).isPresent()) {
            borrowingRepository.deleteById(event.getId());
        }
    }

    @EventHandler
    public void on(UpdateBorrowEvent event) {
        Borrowing borrowing = borrowingRepository.findByEmployeeIdAndBookIdAndReturnDateIsNull(event.getEmployeeId(), event.getBookId());
        borrowing.setReturnDate(event.getReturnDate());
        borrowingRepository.save(borrowing);
    }

    @EventHandler
    public void on(SendMessageEvent event) {
        Message message = new Message(event.getEmployeeId(), event.getMessage());
        borrowService.sendMessage(message);
    }
}
