package com.coucode.borrowservice.command.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Borrowing {
    @Id
    private String id;
    @Column(name = "book_id")
    private String bookId;
    @Column(name = "employee_id")
    private String employeeId;
    @Column(name = "borrowing_date")
    private Date borrowingDate;
    @Column(name = "return_date")
    private Date returnDate;
}
