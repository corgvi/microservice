package com.coucode.commonservice.command;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateStatusBookCommand {
    @TargetAggregateIdentifier
    private String id;
    private Boolean isReady;
    private String employeeId;
    private String borrowId;
}
