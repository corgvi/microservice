package com.coucode.employeeservice.query.controller;

import com.coucode.employeeservice.query.model.EmployeeResponse;
import com.coucode.employeeservice.query.queries.GetAllEmployeeQuery;
import com.coucode.employeeservice.query.queries.GetEmployeeQuery;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/employees")
public class EmployeeQueryController {
    @Autowired
    QueryGateway queryGateway;

    @GetMapping("/{id}")
    public EmployeeResponse getEmployeeDetail(@PathVariable String id) {
        GetEmployeeQuery employeeQuery = new GetEmployeeQuery();
        employeeQuery.setId(id);

        EmployeeResponse employeeResponse = queryGateway.
                query(employeeQuery, ResponseTypes.instanceOf(EmployeeResponse.class)).join();

        return employeeResponse;
    }

    @GetMapping
    public List<EmployeeResponse> findAll() {
        GetAllEmployeeQuery getAllEmployeeQuery = new GetAllEmployeeQuery();
        List<EmployeeResponse> responseList = queryGateway.
                query(getAllEmployeeQuery, ResponseTypes.multipleInstancesOf(EmployeeResponse.class)).join();
        return responseList;
    }
}
