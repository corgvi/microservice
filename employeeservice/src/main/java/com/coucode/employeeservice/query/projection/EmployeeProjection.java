package com.coucode.employeeservice.query.projection;

import com.coucode.employeeservice.command.data.Employee;
import com.coucode.employeeservice.command.data.EmployeeRepository;
import com.coucode.employeeservice.query.model.EmployeeResponse;
import com.coucode.employeeservice.query.queries.GetAllEmployeeQuery;
import com.coucode.employeeservice.query.queries.GetEmployeeQuery;
import org.axonframework.queryhandling.QueryHandler;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

@Component
public class EmployeeProjection {

    @Autowired
    private EmployeeRepository employeeRepository;

    @QueryHandler
    public EmployeeResponse handle(GetEmployeeQuery employeeQuery) {
        EmployeeResponse employeeResponse = new EmployeeResponse();
        Employee employee = employeeRepository.findById(employeeQuery.getId()).orElseThrow(() -> new EntityNotFoundException());
        BeanUtils.copyProperties(employee, employeeResponse);
        return employeeResponse;
    }

    @QueryHandler
    public List<EmployeeResponse> handle(GetAllEmployeeQuery getAllEmployeeQuery) {
        List<EmployeeResponse>  responseList = new ArrayList<>();
        List<Employee> employees = employeeRepository.findAll();
        employees.forEach(employee -> {
            EmployeeResponse employeeResponse = new EmployeeResponse();
            BeanUtils.copyProperties(employee, employeeResponse);
            responseList.add(employeeResponse);
        });
        return responseList;
    }
}
