package com.coucode.employeeservice.command.controller;

import com.coucode.employeeservice.command.command.CreateEmployeeCommand;
import com.coucode.employeeservice.command.command.DeleteEmployeeCommand;
import com.coucode.employeeservice.command.command.UpdateEmployeeCommand;
import com.coucode.employeeservice.command.model.EmployeeRequestModel;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.*;

import javax.xml.transform.Source;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/employees")
@EnableBinding(Source.class)
public class EmployeeCommandController {
    @Autowired
    private CommandGateway commandGateway;

    @Qualifier("errorChannel")
    @Autowired
    private MessageChannel messageChannel;

    @PostMapping
    public CreateEmployeeCommand addBook(@RequestBody EmployeeRequestModel employeeRequestModel){
        // đóng gói BookRequestModel thành 1 message
        CreateEmployeeCommand createEmployeeCommand = new CreateEmployeeCommand();
        createEmployeeCommand.setLastName(employeeRequestModel.getLastName());
        createEmployeeCommand.setId(UUID.randomUUID().toString());
        createEmployeeCommand.setKin(employeeRequestModel.getKin());
        createEmployeeCommand.setFirstName(employeeRequestModel.getFirstName());
        createEmployeeCommand.setIsDisciplined(employeeRequestModel.getIsDisciplined());
        // gửi đi
        commandGateway.sendAndWait(createEmployeeCommand);
        return createEmployeeCommand;
    }

    @PutMapping("/{id}")
    public UpdateEmployeeCommand updateBook(@RequestBody EmployeeRequestModel employeeRequestModel, @PathVariable String id){
        // đóng gói BookRequestModel thành 1 message

        UpdateEmployeeCommand updateEmployeeCommand = new UpdateEmployeeCommand();
        updateEmployeeCommand.setLastName(employeeRequestModel.getLastName());
        updateEmployeeCommand.setId(id);
        updateEmployeeCommand.setKin(employeeRequestModel.getKin());
        updateEmployeeCommand.setFirstName(employeeRequestModel.getFirstName());
        updateEmployeeCommand.setIsDisciplined(employeeRequestModel.getIsDisciplined());
        // gửi đi
        commandGateway.sendAndWait(updateEmployeeCommand);
        return updateEmployeeCommand;
    }

    @DeleteMapping("/{id}")
    public DeleteEmployeeCommand deleteBook(@PathVariable String id) {
        DeleteEmployeeCommand deleteEmployeeCommand = new DeleteEmployeeCommand(id);
        commandGateway.sendAndWait(deleteEmployeeCommand);
        return deleteEmployeeCommand;
    }

    @PostMapping("/sendMessage")
    public void SendMessage(@RequestBody String message) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(message);
            messageChannel.send(MessageBuilder.withPayload(json).build());
        }catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
