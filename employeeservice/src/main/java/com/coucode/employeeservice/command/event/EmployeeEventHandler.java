package com.coucode.employeeservice.command.event;

import com.coucode.employeeservice.command.data.Employee;
import com.coucode.employeeservice.command.data.EmployeeRepository;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;

@Component
public class EmployeeEventHandler {

    @Autowired
    private EmployeeRepository employeeRepository;

    @EventHandler
    public void on (CreateEmployeeEvent createEmployeeEvent) {
        Employee employee = new Employee();
        BeanUtils.copyProperties(createEmployeeEvent, employee);
        System.out.println("oke");
        employeeRepository.save(employee);
    }

    @EventHandler
    public void on (UpdateEmployeeEvent updateEmployeeEvent) {
        Employee employee = employeeRepository.findById(updateEmployeeEvent.getId()).orElseThrow(() -> new EntityNotFoundException());
        employee.setFirstName(updateEmployeeEvent.getFirstName());
        employee.setLastName(updateEmployeeEvent.getLastName());
        employee.setKin(updateEmployeeEvent.getKin());
        employee.setIsDisciplined(updateEmployeeEvent.getIsDisciplined());
        employeeRepository.save(employee);
    }

    @EventHandler
    public void on (DeleteEmployeeEvent deleteEmployeeEvent) {
        if (employeeRepository.findById(deleteEmployeeEvent.getId()).isPresent()) {
            employeeRepository.deleteById(deleteEmployeeEvent.getId());
        }
    }
}
