package com.coucode.employeeservice.command.aggregate;

import com.coucode.employeeservice.command.command.CreateEmployeeCommand;
import com.coucode.employeeservice.command.command.DeleteEmployeeCommand;
import com.coucode.employeeservice.command.command.UpdateEmployeeCommand;
import com.coucode.employeeservice.command.event.CreateEmployeeEvent;
import com.coucode.employeeservice.command.event.DeleteEmployeeEvent;
import com.coucode.employeeservice.command.event.UpdateEmployeeEvent;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.modelling.command.TargetAggregateIdentifier;
import org.axonframework.spring.stereotype.Aggregate;
import org.springframework.beans.BeanUtils;

@Aggregate
public class EmployeeAggregate {
    @AggregateIdentifier
    private String id;
    private String firstName;
    private String lastName;
    private String kin;
    private Boolean isDisciplined;

    public EmployeeAggregate() {
    }
    @CommandHandler
    public EmployeeAggregate(CreateEmployeeCommand createEmployeeCommand) {
        // 1 sự kiện được tạo ra sau khi xử lý lên CreateBookCommand
        CreateEmployeeEvent createEmployeeEvent = new CreateEmployeeEvent();
        // sao chép các thuộc tính từ createBookCommand vào bookCreateEvent
        BeanUtils.copyProperties(createEmployeeCommand, createEmployeeEvent);
        // phát đi event và gọi đến EventSourcingHandler
        AggregateLifecycle.apply(createEmployeeEvent);
    }

    @CommandHandler
    public void handle(UpdateEmployeeCommand updateEmployeeCommand) {
        UpdateEmployeeEvent event = new UpdateEmployeeEvent();
        BeanUtils.copyProperties(updateEmployeeCommand, event);
        AggregateLifecycle.apply(event);
    }

    @CommandHandler
    public void handle(DeleteEmployeeCommand deleteEmployeeCommand) {
        DeleteEmployeeEvent event = new DeleteEmployeeEvent();
        BeanUtils.copyProperties(deleteEmployeeCommand, event);
        AggregateLifecycle.apply(event);
    }

    @EventSourcingHandler
    public void on(CreateEmployeeEvent createEmployeeEvent) {
        this.id = createEmployeeEvent.getId();
        this.firstName = createEmployeeEvent.getFirstName();
        this.kin = createEmployeeEvent.getKin();
        this.isDisciplined = createEmployeeEvent.getIsDisciplined();
        this.lastName = createEmployeeEvent.getLastName();
    }

    @EventSourcingHandler
    public void on(UpdateEmployeeEvent updateEmployeeEvent) {
        this.id = updateEmployeeEvent.getId();
        this.firstName = updateEmployeeEvent.getFirstName();
        this.kin = updateEmployeeEvent.getKin();
        this.isDisciplined = updateEmployeeEvent.getIsDisciplined();
        this.lastName = updateEmployeeEvent.getLastName();
    }

    @EventSourcingHandler
    public void on(DeleteEmployeeEvent deleteEmployeeEvent) {
        this.id = deleteEmployeeEvent.getId();
    }
}
