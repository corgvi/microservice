package com.coucode.employeeservice.command.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeRequestModel {
    private String firstName;
    private String lastName;
    private String kin;
    private Boolean isDisciplined;
}
